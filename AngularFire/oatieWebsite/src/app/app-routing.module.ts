import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './services/auth.guard';

import { LoginComponent } from './login/login.component';
import { PhotoAlbumComponent } from './photo-album/photo-album.component';

const routes: Routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full' },           // redirect the root to 'login'
    { path: 'login', component: LoginComponent },
    { path: 'photoAlbum', component: PhotoAlbumComponent, canActivate: [AuthGuard] },
    { path: '**', component: LoginComponent }                       // redirect everything else to 'login'
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
