import { Component, OnInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    isProgressVisible: boolean;
    email: string;
    password: string;

    constructor(
        private authService: AuthService,
        private router: Router) {

        this.isProgressVisible = false;
        this.email = 'ryan@gmail.com';
        this.password = '123456';
    }

    ngOnInit() { }

    loginUser() {
        this.isProgressVisible = true;                          // show the progress indicator as we start the Firebase login process

        this.authService.loginUser(this.email, this.password).then((result) => {

            this.isProgressVisible = false;                     // no matter what, when the auth service returns, we hide the progress indicator

            if (result == null) {                               // null is success, false means there was an error
                console.log('login worked!!');
                this.router.navigate(['/photoAlbum']);
            }
            else if (result == false) {
                console.log('login error!!!');

            }
        });
    }

}
