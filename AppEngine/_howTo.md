log in to https://console.cloud.google.com/


################### making a simple website ########################

create this structure locally:
    oatieWebsite/
    ├── app.yaml
    ├── www/
        ├── index.html
        ├── css/style.css
        ├── js/script.js
        ├── img/
        ├── other dir

Get ready to deploy to App Engine

    install Google Cloud SDK
        download at: https://cloud.google.com/sdk/docs/

    if you already have the Google Cloud SDK installed, update it
        gcloud components update

    check your Google Cloud version
        gcloud --version

    terminal
        cd oatieWebsite

        I need to make sure I'm logged in as the correct user
            gcloud auth login
                (choose the right account when prompted)


        we can either create a new project in the SDK on the command line like this:
            gcloud projects create [YOUR_PROJECT_ID] --set-as-default

        ...or we can choose an existing project to get the PROJECT_NUMBER of 'MyGDGSite'...
            gcloud projects list


gcloud app deploy --project mygdgsite
gcloud app browse --project=mygdgsite




################### making a Python REST app / Flask server ########################

create this structure locally:          (these are the absolute basic, required files)
    server/
    ├── app.yaml
    ├── main.py
    ├── requirements.txt

    the 'server' directory can have any name, but the YAML, PY, and TXT files all need those exact names

Make the Python server (we'll deploy it to Google's App Engine later on)
------------------------------------------------------------------------

    * Create the necessary files:
        mkdir server
        cd server

        create requirements.txt
        create app.yaml
        create .gcloudignore
        create main.py

        mkdir templates
            populate the templates dir with html, js, and css files

    * Test the server locally:
        python -m venv env                      # create a virtual environment to mimic the clean environment on App Engine!
        source env/bin/activate                 # activate the virtual environment

        pip install -r requirements.txt 
        python main.py                          # now you can browse to http://localhost:8088

    * After you finish testing, turn off the virtual environment
        control+c
        deactivate



Deploy the Python server to App Engine
--------------------------------------

    install Google Cloud SDK
        download at: https://cloud.google.com/sdk/docs/

    if you already have the Google Cloud SDK installed, update it
        gcloud components update

    check your Google Cloud version
        gcloud --version

    terminal
        cd server

        I need to make sure I'm logged in as the correct user
            gcloud auth login
                (choose the right account when prompted)


        we can either create a new project in the SDK on the command line like this:
            gcloud projects create [YOUR_PROJECT_ID] --set-as-default

        ...or we can choose an existing project to get the PROJECT_NUMBER of 'MyGDGSite'...
            gcloud projects list


gcloud app deploy --project mygdgsite
gcloud app browse --project=mygdgsite









